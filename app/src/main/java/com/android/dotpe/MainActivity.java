package com.android.dotpe;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.dotpe.core.ProductContractor;
import com.android.dotpe.core.ProductPresenter;
import com.android.dotpe.response.Item;
import com.android.dotpe.response.ProductData;
import com.android.dotpe.response.ProductResponse;
import com.android.dotpe.response.dummy.CategorisedProduct;
import com.android.dotpe.response.dummy.CategorisedProductData;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ProductContractor.View {
    private ProductPresenter productPresenter;
    private List<ProductData> productDataList = new ArrayList<>();
    private ImageView ivBack;
    private RecyclerView recyclerView;
    private ProductAdapter productAdapter;
//    private CategorisedProductData categorisedProduct;
    private List<CategorisedProductData> categorisedProductsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        productPresenter = new ProductPresenter(this);
        initViews();
        getProducts();
    }

    private void initViews() {
        ivBack = findViewById(R.id.iv_back);
        recyclerView = findViewById(R.id.recycler_view);

        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
    }

    private void getProducts() {
        productPresenter.productRequest();
    }

    @Override
    public void onProductResponse(ProductResponse response) {
        if (response != null) {
            if (response.isStatus()) {
                productDataList = response.getProductDataList();
                for (int i = 0; i < productDataList.size(); i++) {
                    for (Item item : productDataList.get(i).getItems()) {
                        CategorisedProductData categorisedProduct = new CategorisedProductData(
                                item.getCategory().getName(),
                                item.getCategory().getId(),
                                item.getName(),
                                item.getPrice(),
                                item.getImageUrl(),
                                item.getAvailable());
                        categorisedProductsList.add(categorisedProduct);
                    }

                }
                Log.e("SS", categorisedProductsList.size()+"");
                productAdapter = new ProductAdapter(this, categorisedProductsList);
                recyclerView.setAdapter(productAdapter);
            }else
                Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}