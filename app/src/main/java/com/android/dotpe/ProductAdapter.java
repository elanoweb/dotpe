package com.android.dotpe;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dotpe.response.ProductData;
import com.android.dotpe.response.dummy.CategorisedProductData;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {
    Context context;
    List<CategorisedProductData> productDataList;


    public ProductAdapter(Context context, List<CategorisedProductData> productDataList) {
        this.context = context;
        this.productDataList = productDataList;

    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View convertView;
        convertView = LayoutInflater.from(context).inflate(R.layout.adapter_product_layout, parent, false);
        ProductViewHolder viewHolder = new ProductViewHolder(convertView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Log.e("PRO", productDataList.get(position).getItemName() + " " + productDataList.size());
        holder.tvItemName.setText(productDataList.get(position).getItemName());
    }

    @Override
    public int getItemCount() {
        return productDataList.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        private TextView tvItemName, tvAdd;
        ImageView ivItem;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            tvAdd = itemView.findViewById(R.id.tv_add);
            tvItemName = itemView.findViewById(R.id.tv_item_name);
            ivItem = itemView.findViewById(R.id.iv_item);

        }
    }
}
