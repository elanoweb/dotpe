package com.android.dotpe.core;

import com.android.dotpe.response.ProductResponse;

public interface ProductContractor {
    interface View {
        void onProductResponse(ProductResponse response);
    }
    interface Presenter {
        void onProductResponse(ProductResponse response);
        void productRequest();
    }
    interface Interactor {
        void productRequest();
    }
}
