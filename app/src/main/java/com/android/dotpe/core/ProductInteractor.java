package com.android.dotpe.core;

import com.android.dotpe.Utils.Constants;
import com.android.dotpe.remote.Apis;
import com.android.dotpe.remote.WebServices;
import com.android.dotpe.response.ProductResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductInteractor implements ProductContractor.Interactor {
    private ProductPresenter productPresenter;

    public ProductInteractor(ProductPresenter productPresenter) {
        this.productPresenter = productPresenter;
    }

    @Override
    public void productRequest() {
        WebServices webServices = Apis.getRetrofitClient(Constants.BASE_URL).create(WebServices.class);
        Call<ProductResponse> call = webServices.getProduct();
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (response.isSuccessful())
                    productPresenter.onProductResponse(response.body());
                else
                    productPresenter.onProductResponse(null);
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                productPresenter.onProductResponse(null);
            }
        });
    }
}
