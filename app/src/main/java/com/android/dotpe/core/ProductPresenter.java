package com.android.dotpe.core;

import com.android.dotpe.response.ProductResponse;

public class ProductPresenter implements ProductContractor.Presenter {
    private ProductContractor.View mView;
    private ProductInteractor interactor;

    public ProductPresenter(ProductContractor.View mView) {
        this.mView = mView;
        interactor = new ProductInteractor(this);
    }

    @Override
    public void onProductResponse(ProductResponse response) {
        mView.onProductResponse(response);
    }

    @Override
    public void productRequest() {
        interactor.productRequest();
    }
}
