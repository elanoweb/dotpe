package com.android.dotpe.remote;

import com.android.dotpe.response.ProductResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface WebServices {
    @GET("catalog/getItems/2018")
    Call<ProductResponse> getProduct();

}
