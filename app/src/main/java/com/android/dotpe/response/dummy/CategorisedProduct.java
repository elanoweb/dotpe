package com.android.dotpe.response.dummy;

public class CategorisedProduct {
    private CategorisedProductData categorisedProductData;

    public CategorisedProductData getCategorisedProductData() {
        return categorisedProductData;
    }

    public void setCategorisedProductData(CategorisedProductData categorisedProductData) {
        this.categorisedProductData = categorisedProductData;
    }
}
