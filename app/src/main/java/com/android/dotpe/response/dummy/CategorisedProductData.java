package com.android.dotpe.response.dummy;

public class CategorisedProductData {
    private String catName;
    private int catId;
    private String itemName;
    private int price;
    private String imageUrl;
    private int available;

    public CategorisedProductData(String catName, int catId, String itemName, int price, String imageUrl, int available) {
        this.catName = catName;
        this.catId = catId;
        this.itemName = itemName;
        this.price = price;
        this.imageUrl = imageUrl;
        this.available = available;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }
}
